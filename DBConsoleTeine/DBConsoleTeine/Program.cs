﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBConsoleTeine
{
    class Program
    {
        static void Main(string[] args)
        {
            NorthwindEntities2 ne = new NorthwindEntities2();

            foreach(var x in ne.Employees)
            {
                Console.WriteLine(x.FirstName);
            }

            foreach(var x in ne.Products)
            {
                Console.WriteLine($"{x.ProductName} maksab {x.UnitPrice:F2}");
            }

        }
    }
}
