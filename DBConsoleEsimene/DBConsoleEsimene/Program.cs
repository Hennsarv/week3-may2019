﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;


namespace DBConsoleEsimene
{
    class Employee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public override string ToString() => $"{FirstName} {LastName}";
    }

    class Program
    {
        static void Main(string[] args)
        {
            // kus on andmebaas kuhu end ühendada
            //string connString = "Data Source=valiitsql.database.windows.net;Initial Catalog=NOrthwind;User ID=Student;Password=Pa$$w0rd";

            // loome ühenduse
            SqlConnectionStringBuilder sb = new SqlConnectionStringBuilder
            {
                DataSource = "Valiitsql.database.windows.net",
                UserID = "Student",
                Password = "Pa$$w0rd",
                InitialCatalog = "Northwind"
            };
            string connString = sb.ConnectionString;
            
            SqlConnection conn = new SqlConnection(connString);
            conn.Open();

            // teeme sql commandi e. küsimus andmebaasile
            SqlCommand comm = new SqlCommand("select FirstName, LastName from Employees", conn);

            var reader = comm.ExecuteReader();

            // loen readeri läbi
            List<Employee> employees = new List<Employee>();
            while(reader.Read())
            {
                //Console.WriteLine($"{reader["FirstName"]} {reader["LastName"]}");
                employees.Add(new Employee { FirstName = reader.GetString(0), LastName = reader.GetString(1) });
            }

            foreach (var x in employees.OrderBy(x => x.LastName)) Console.WriteLine(x);
        }
    }
}
