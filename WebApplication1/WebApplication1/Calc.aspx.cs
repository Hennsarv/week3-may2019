﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Numerics;


namespace WebApplication1
{
    public partial class Calc : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Liitmine(object sender, EventArgs e)
        {
            Arvuta((x, y) => x + y);
        }
        protected void Lahutamine(object sender, EventArgs e)
        {
            Arvuta((x, y) => x - y);
        }
        protected void Korrutamine(object sender, EventArgs e)
        {
            Arvuta((x, y) => x * y);
        }
        protected void Jagamine(object sender, EventArgs e)
        {
            Arvuta((x, y) => x / y);
        }
        void Arvuta(Func<BigInteger,BigInteger,BigInteger> f )
        {
            this.Vastus.Text = $"{f(BigInteger.Parse(Arv1.Text), BigInteger.Parse(Arv2.Text))}";

        }
    }
}