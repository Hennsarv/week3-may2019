﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Calc.aspx.cs" Inherits="WebApplication1.Calc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Henn oskab arvutada</title>
</head>
<body>
    <h1>Hennu arvutusleht</h1>
    <form id="form1" runat="server">
        <div>

            <asp:TextBox ID="Arv1" runat="server" Width="500px"></asp:TextBox><br />
            <asp:TextBox ID="Arv2" runat="server" Width="500px"></asp:TextBox>

            <br />
            <asp:Button ID="Liida" runat="server" Text="+" Font-Size="XX-Large" OnClick="Liitmine" />
            <asp:Button ID="Lahuta" runat="server" Text="-" Font-Size="XX-Large"  OnClick="Lahutamine" />
            <asp:Button ID="Korruta" runat="server" Text="*" Font-Size="XX-Large"  OnClick="Korrutamine" />
            <asp:Button ID="Jaga" runat="server" Text="/" Font-Size="XX-Large"  OnClick="Jagamine" />
            <br />

            <asp:Label ID="Vastus" runat="server" Text="Label" Font-Size="XX-Large">Vastus</asp:Label>
        </div>
    </form>
</body>
</html>
