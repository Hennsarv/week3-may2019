﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            NorthwindEntities ne = new NorthwindEntities();
            ne.Categories.ToList().ForEach(x => Console.WriteLine(x.CategoryName));
        }
    }
}
