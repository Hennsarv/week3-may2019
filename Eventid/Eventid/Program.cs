﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Eventid
{
    class Program
    {
        static void Pidu(Inimene x)
        {
            Console.WriteLine($"{x.Nimi} puhul saab pidu! ta saab {x.Vanus} aastat vanaks");
        }

        static int Korruta(int x, int y) => x * y;

        static void Main(string[] args)
        {

            int õ = 7;

            Func<int, int, int> f2 = (x, y) => x * y + õ;  // pikalt kirjutatud lokaalne funktsioon
            int f1(int x, int y) => x * y + õ;             // sama lühidalt kirjutatud
            void p1(int x) => Console.WriteLine(x);        // lokaalne anonüümne meetod lühike
            Action<int> p2 = x => Console.WriteLine(x);    // sama asi pikalt kirjutatuna

            //Console.WriteLine(f(3,4));
            //Console.WriteLine(f);
            p1(f1(7, 8));

            Expression<Func<int, int, int>> e = (x, y) => x * y;
            Console.WriteLine(e);
            var ef = e.Compile();
            p1(ef(9, 10));
            p1(e.Compile()(11, 13));

            Console.WriteLine("Tere maailm!");
            Inimene h = new Inimene { Nimi = "Henn", Vanus = 60 };
            Inimene a = new Inimene { Nimi = "Ants", Vanus = 63 };

            // lisame objektile event handler - sündmuse käsitleja
            h.Pensionile += SaadaPensile;
            h.Pensionile += Pidu;
            h.Juubel += x => Console.WriteLine($"Keegi peaks {x.Nimi} jaoks kingituse otsima");
            h.Juubel += Pidu;
            a.Pensionile += SaadaPensile; //x => Console.WriteLine($"{x.Nimi} mine sa ka pensionile, mis tolgendad");
            a.Juubel += Pidu;

            

            for (int i = 0; i < 20; i++)
            {
                h.Vanus++;
                a.Vanus++;
                Console.WriteLine(h);

            }

        }

        private static void SaadaPensile(Inimene x)
        {
            Console.WriteLine($"{x.Nimi} sul on aeg pensionile minna");
        }
    }

    class Inimene
    {

        public event Action<Inimene> Pensionile;    // sündmus
        public event Action<Inimene> Juubel;        // sündmus


        public string Nimi { get; set; }

        private int _Vanus;
        public int Vanus
        {
            get => _Vanus;
            set
            {
                _Vanus = value;

                if (_Vanus == 65)
                {
                    //if (Pensionile != null) Pensionile(this);
                    Pensionile?.Invoke(this);
                }

                if (_Vanus == 75) Juubel?.Invoke(this);
            }
        }

        public override string ToString() => $"{Nimi} on {Vanus} aastane";
    }
}
