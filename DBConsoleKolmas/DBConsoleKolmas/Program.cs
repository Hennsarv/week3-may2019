﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBConsoleKolmas
{
    partial class Employee
    {
        public string FullName => $"{FirstName} {LastName}";
    }


    class Program
    {
        static void Main(string[] args)
        {
            NorthwindEntities ne = new NorthwindEntities();
            //foreach (var c in ne.Categories)
            //{
            //    Console.WriteLine($" meil on kategooria {c.CategoryName} ja selles on tooted:");
            //    foreach(var p in c.Products)
            //    {
            //        Console.WriteLine($"\t{p.ProductName} hinnaga {p.UnitPrice:F3}");
            //    }
            //}
            //foreach(var p in ne.Products)
            //    Console.WriteLine($"{p.ProductName} kategoorias {p.Category?.CategoryName??"ppuudub"}");

            //foreach(var e in ne.Employees)
            //    Console.WriteLine($"{e.FullName} ülemus on {e.Manager?.FullName??"ise jumal"}");

            //ne.Database.Log = Console.WriteLine;

            foreach(var p in ne.Products
                .Where(p => p.UnitPrice > 50)
                .OrderBy(p => p.UnitPrice)
                ) Console.WriteLine($"{p.ProductName} \t{p.UnitPrice:F2}");



            Category c = ne.Categories.Find(8);
            Console.WriteLine(c?.CategoryName??"Sellist pole");

            // muudame 8 kategooria nime ära
            c.CategoryName = "Seatoit";
            ne.SaveChanges();


            //if (c != null)
            //    c.Products.ToList().ForEach(p => p.UnitPrice /= 2);

            //ne.SaveChanges();


            foreach (var p in ne.Products.Where(p => p.ProductName.Contains("Krok")))
                ne.Products.Remove(p);

            ne.SaveChanges();

            ne.Database.Log = Console.WriteLine;


            var q = from p in ne.Products
                    where p.UnitPrice > 50
                    orderby p.ProductName descending
                    select new { p.ProductName, p.UnitPrice };

            var q1 = ne.Products.Where(p => p.UnitPrice > 50).OrderByDescending(p => p.ProductName)
                .Select(p => new { p.ProductName, p.UnitPrice });


            foreach (var p in q1) Console.WriteLine(p);

            var k = (from p in ne.Products where p.CategoryID == 8 select p.UnitPrice).Average();
            Console.WriteLine(k);




        }
    }
}
