﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EsimeneAken
{
    class Inimene
    {
        public string Nimi { get; set; }
        public int Vanus { get; set; }
        public int? Kinganumber { get; set; }
        public int? SabadeArv { get; set; }
    }

    static class Program
    {
        public static string Nimi = "Henn";

        public static List<Inimene> Inimesed = new List<Inimene>();


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {



            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
