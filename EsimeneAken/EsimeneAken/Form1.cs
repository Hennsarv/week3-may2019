﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;

namespace EsimeneAken
{
    public partial class Form1 : Form
    {
        string filename = @"..\..\inimesed.txt";
        string filenameJson = @"..\..\inimesed.json";

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.Inimesed = new List<Inimene>
            {
            new Inimene {Nimi = "Henn", Vanus = 64, Kinganumber = 45},
            new Inimene {Nimi = "Ants", Vanus = 40, Kinganumber = 41},
            new Inimene {Nimi = "Peeter", Vanus = 28, Kinganumber = 42},
            new Inimene {Nimi = "Jaak", Vanus = 33, Kinganumber = 49},
            new Inimene {Nimi = "Kalle", Vanus = 44, Kinganumber = 36},
            };


            this.dataGridView1.DataSource = null;
            this.dataGridView1.DataSource = Program.Inimesed;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Program.Inimesed.Add(
             new Inimene
             {
                 Nimi = this.textBox1.Text,
                 Vanus = int.Parse(this.textBox2.Text),
                 Kinganumber = int.Parse(this.textBox3.Text)
             });

            this.dataGridView1.DataSource = null;
            this.dataGridView1.DataSource = Program.Inimesed;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            File.WriteAllText(filenameJson,
            JsonConvert.SerializeObject(Program.Inimesed));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //Program.Inimesed =
            //File.ReadAllLines(filename)
            //    .Select(x => x.Split(','))
            //    .Select(x => new Inimene { Nimi = x[0], Vanus = int.Parse(x[1]), Kinganumber = int.Parse(x[2]) })
            //    .ToList();

            Program.Inimesed =
            JsonConvert.DeserializeObject<List<Inimene>>(File.ReadAllText(filenameJson));

            this.dataGridView1.DataSource = null;
            this.dataGridView1.DataSource = Program.Inimesed;
        }
    }
}
