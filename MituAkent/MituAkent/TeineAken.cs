﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MituAkent
{
    public partial class TeineAken : Form
    {
        public string KelleNimi { get; set; }

        public TeineAken()
        {
            InitializeComponent();
            
        }

        private void TeineAken_Load(object sender, EventArgs e)
        {
            this.label1.Text = $"selle aknas on {KelleNimi} andmed";
        }

        private void TeineAken_Resize(object sender, EventArgs e)
        {
            MessageBox.Show("ära sikuta servast");
            
        }

        private void TeineAken_FormClosed(object sender, FormClosedEventArgs e)
        {
            MessageBox.Show("aken pandi kinni");
            
        }

        private void TeineAken_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
        }
    }
}
