﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBAknaga2
{
    static class Program
    {
        //public static IEnumerable<T> Union<T>(this IEnumerable<T> esimene, IEnumerable<T> teine)
        //{
        //    foreach (T x in esimene) yield return x;
        //    foreach (T x in teine) yield return x;
        //}


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //int[] arvud1 = { 1, 2, 3, 4, 5 };
            //int[] arvud2 = { 77, 83 };

            //var koos = arvud1.Union(arvud2).ToList();


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Teine());
        }
    }
}
