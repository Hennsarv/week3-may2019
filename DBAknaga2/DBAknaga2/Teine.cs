﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBAknaga2
{
    public partial class Teine : Form
    {
        string allCountries = "Kõik";

        public Teine()
        {
            InitializeComponent();
        }

        private void Teine_Load(object sender, EventArgs e)
        {
            NorthwindEntities ne = new NorthwindEntities();

            var maad = 
                ne.Customers.Select(c => c.Country).Distinct().ToList();

            //maad.Add(allCountries);
            //maad.Sort();

            this.listBox1.DataSource =
                (new List<string> { allCountries }).Union(maad).ToList();
            //maad;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string country = 
            this.listBox1.SelectedValue.ToString();

            NorthwindEntities ne = new NorthwindEntities();

            if (country == allCountries)
                this.dataGridView1.DataSource = ne.Customers.ToList();
            else
            this.dataGridView1.DataSource =
                ne.Customers.Where(c => c.Country == country).ToList();
        }
    }
}
