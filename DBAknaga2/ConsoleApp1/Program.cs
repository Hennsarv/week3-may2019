﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Henn\MinuDB.mdf;Integrated Security=True;Connect Timeout=30");
            conn.Open();

            SqlCommand comm = new SqlCommand("select * from [Table]", conn);
            var R = comm.ExecuteReader();
            while(R.Read())
                Console.WriteLine(R["Nimi"]);

            Console.WriteLine("\nsama asi enityga\n");

            MinuDBEntities db = new MinuDBEntities();


            while (true)
            {
                foreach (var t in db.Table)
                    Console.WriteLine($"{t.Nimi} {t.Vanus}");

                Console.Write("anna nimi, vanus ja kinganumber: ");
                string[] rida = Console.ReadLine().Split(',');
                if (rida.Length < 3) break;
                db.Table.Add(new Table
                {
                    Nimi = rida[0],
                    Vanus = int.Parse(rida[1]),
                    Kinganumber = int.Parse(rida[2])
                });
                db.SaveChanges();

            }

        }
    }
}
