﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MVCEsimene.Models
{
    public class Inimene
    {
        public static List<Inimene> Inimesed = new List<Inimene>();

        static Inimene()
        {
            Inimesed.Add(new Inimene { Nimi = "Henn", Vanus = 64 });
        }

        public static int loendur = 0;

        public int Nr { get; private set; } = ++loendur;
        public string Nimi { get; set; }

        [Display(Name = "Age")]
        public int Vanus { get; set; }

    }
}