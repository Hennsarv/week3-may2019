﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCEsimene.Models;

namespace MVCEsimene.Controllers
{
    public class InimeneController : Controller
    {
        // GET: Inimene
        public ActionResult Index()
        {
            
            return View(Inimene.Inimesed);
        }

        // GET: Inimene/Details/5
        public ActionResult Details(int id)
        {
            Inimene i = Inimene.Inimesed.Where(x => x.Nr == id).SingleOrDefault();
            if (i != null) return View(i);
            return RedirectToAction("Index");
            
        }

        // GET: Inimene/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inimene/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                Inimene.Inimesed.Add(new Inimene
                {
                    Nimi = collection["Nimi"],
                    Vanus = int.Parse(collection["Vanus"])
                }

                    );

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimene/Edit/5
        public ActionResult Edit(int id)
        {
            return View(Inimene.Inimesed.Where(x => x.Nr == id).SingleOrDefault());
        }

        // POST: Inimene/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                Inimene i = Inimene.Inimesed.Where(x => x.Nr == id).SingleOrDefault();
                if (i != null)
                    i.Nimi = collection["Nimi"];

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Inimene/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Inimene/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
